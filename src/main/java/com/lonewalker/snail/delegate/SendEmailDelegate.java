package com.lonewalker.snail.delegate;

import com.lonewalker.snail.service.MailService;
import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

/**
 * @author lonewalker
 */
@RequiredArgsConstructor
@Component
public class SendEmailDelegate implements JavaDelegate {

    private final MailService mailService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String initiator = (String) execution.getVariable("initiator");
        String processInstanceId = execution.getProcessInstanceId();
        String content = initiator+"--发起的审批流程抄送给您["+processInstanceId+"]";
        //改成自己的邮箱账号调试
        mailService.sendSimpleMail("xxx@qq.com","审批抄送",content);
    }
}
