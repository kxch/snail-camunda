package com.lonewalker.snail.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.camunda.bpm.engine.impl.persistence.entity.HistoricActivityInstanceEntity;

import java.util.List;

/**
 * @author lonewalker
 */
public interface HisActivityMapper extends BaseMapper<HistoricActivityInstanceEntity> {

    /**
     * 获取序列计数器
     *
     * @param processInstanceId 流程实例id
     * @param activityId        环节id
     * @return 计数器
     */
    Integer getBodySequenceCounter(@Param("processInstanceId") String processInstanceId,
                                   @Param("activityId") String activityId);


    /**
     * 获取完成的节点id
     *
     * @param processInstanceId 流程实例id
     * @param sequenceCounter   序列计数器
     * @return 节点Id
     */
    List<String> getCompletionNodes(@Param("processInstanceId") String processInstanceId,
                                    @Param("sequenceCounter") Integer sequenceCounter);
}
