package com.lonewalker.snail.domain.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lonewalker
 */
@Data
public class TaskVo implements Serializable {
    private static final long serialVersionUID = -7053911687692749915L;


    private String processInstanceId;

    private String taskId;

}
