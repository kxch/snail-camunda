package com.lonewalker.snail.domain.request;

import lombok.Data;

/**
 * @description:
 * @author: LoneWalker
 * @create: 2022-12-23
 **/
@Data
public class AddBusinessRequest {

    private Long id;

    private String name;

    private String processDefinitionKey;
}
