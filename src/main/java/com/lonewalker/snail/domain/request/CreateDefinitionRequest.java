package com.lonewalker.snail.domain.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * @author lonewalker
 */
@Data
public class CreateDefinitionRequest {

    /**
     * 流程定义key
     */
    @NotBlank(message = "流程定义key不能为空")
    private String processDefinitionKey;

    /**
     * 流程定义数据
     */
    @NotEmpty(message = "流程定义不能为空")
    private String processDefinition;
}
