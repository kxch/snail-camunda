package com.lonewalker.snail.domain.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author lonewalker
 */
@Data
public class SetVariableRequest {

    /**
     * 流程实例id
     */
    @NotBlank(message = "流程实例id不能为空")
    private String processInstanceId;

}
