package com.lonewalker.snail.domain.request;

import com.lonewalker.snail.constant.OperationTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 变更实例参数类
 * @author wangyu
 */
@Data
public class ModifyInstanceRequest {

    /**
     * 流程实例id
     */
    @NotBlank(message = "流程实例id不能为空")
    private String processInstanceId;

    /**
     * 操作类型 {@link OperationTypeEnum}
     */
    @NotNull(message = "操作类型不能为空")
    private Integer operationType;

    /**
     * 目标节点id
     */
    @NotBlank(message = "目标节点id不能为空")
    private String targetNodeId;
}
