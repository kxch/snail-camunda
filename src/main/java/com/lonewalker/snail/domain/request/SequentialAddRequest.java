package com.lonewalker.snail.domain.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 顺序加签相关参数
 *
 * @author LoneWalker
 */
@Data
public class SequentialAddRequest {
    @NotBlank(message = "流程实例id不能为空")
    private String processInstanceId;

    @NotBlank(message = "节点id不能为空")
    private String nodeId;
}
