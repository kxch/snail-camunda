package com.lonewalker.snail.domain.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 添加任务分配人请求参数类
 * @author lonewalker
 */
@Data
public class AddAssigneeRequest {

    @NotBlank(message = "流程实例id不能为空")
    private String processInstanceId;

    @NotBlank(message = "节点id不能为空")
    private String nodeId;

    @NotBlank(message = "加签人不能为空")
    private String userId;
}
