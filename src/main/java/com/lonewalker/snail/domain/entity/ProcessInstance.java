package com.lonewalker.snail.domain.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @description: 流程实例信息
 * @author: LoneWalker
 **/
@EqualsAndHashCode(callSuper = true)
@Data
public class ProcessInstance extends BaseEntity {

    private static final long serialVersionUID = -3447979884795929898L;
    private Long id;

    /**
     * 关联的业务主键
     */
    private Long businessId;
    /**
     * 业务类型 {@link com.lonewalker.snail.constant.BusinessTypeEnum}
     */
    private Integer businessType;
    /**
     * 业务Key
     */
    private String businessKey;
    /**
     * 模块id
     */
    private Long moduleId;
    /**
     * 流程定义key
     */
    private String processDefinitionKey;

    /**
     * 流程定义版本
     */
    private Integer processDefinitionVersion;

    /**
     * 流程实例id
     */
    private String processInstanceId;

    /**
     * 流程状态
     */
    private Integer processState;

    /**
     * 当前节点id
     */
    private String currentNodeId;

    /**
     * 当前节点名称
     */
    private String currentNodeName;
}
