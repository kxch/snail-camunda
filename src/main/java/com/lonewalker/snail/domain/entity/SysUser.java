package com.lonewalker.snail.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: 用户信息
 * @author: LoneWalker
 * @create: 2022-12-23
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SysUser {

    private Long id;
    /**
     * 用户账号
     */
    private String userName;
    /**
     * 中文姓名
     */
    private String realName;
    /**
     * 租户Id
     */
    private Long tenantId;
}
