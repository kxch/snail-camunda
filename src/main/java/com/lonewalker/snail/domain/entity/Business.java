package com.lonewalker.snail.domain.entity;

import lombok.Data;

/**
 * 业务实体类
 * @author LoneWalker
 */
@Data
public class Business {

    private Long id;

    private String name;

    private String processInstanceId;
}
