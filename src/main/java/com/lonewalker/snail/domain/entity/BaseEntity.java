package com.lonewalker.snail.domain.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description: 基类
 * @author: lonewalker
 * @create: 2022-12-23
 **/
@Data
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 3669128450880843182L;
    /**
     * 租户Id
     */
    private Long tenantId;

    /**
     * 创建人id
     */
    private Long createById;

    /**
     * 创建人名称
     */
    private String createByName;

    /**
     * 发起时间
     */
    private Date createTime;

    /**
     * 修改人id
     */
    private Long updateById;

    /**
     * 修改时间
     */
    private Date updateTime;
}
