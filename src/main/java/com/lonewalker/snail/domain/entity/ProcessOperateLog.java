package com.lonewalker.snail.domain.entity;

import lombok.Data;

/**
 * 流程操作日志表
 * @author LoneWalker
 */
@Data
public class ProcessOperateLog extends BaseEntity {
    private static final long serialVersionUID = 1095845432297095697L;
    private Long id;
    /**
     * 流程实例id
     */
    private String processInstanceId;
    /**
     * 模块类型
     */
    private Integer moduleType;
    /**
     * 业务类型
     */
    private Integer businessType;
    /**
     * 业务名称
     */
    private String businessName;
    /**
     * 操作节点id
     */
    private String nodeId;
    /**
     * 操作节点名称
     */
    private String nodeName;
    /**
     * 任务id
     */
    private String taskId;
    /**
     * 审批意见
     */
    private String opinion;
    /**
     * 下一节点id
     */
    private String nextNodeId;
    /**
     * 租户id
     */
    private Long tenantId;
}
