package com.lonewalker.snail.constant;

import lombok.Getter;

/**
 * @author LoneWalker
 */

@Getter
public enum BusinessTypeEnum {
    /**
     * 请假
     */
    ASK_OFF(1,"askOff","请假申请"),
    /**
     * 加班
     */
    OVERTIME(2,"overTime","加班申请"),
    /**
     * 转正
     */
    FORMAL(3,"formal","转正申请"),

    /**
     * 离职
     */
    LEAVE(4,"leave","离职申请"),


    ;

    private Integer code;
    /**
     * 业务别名
     */
    private String alias;

    private String desc;


    BusinessTypeEnum(Integer code,String alias,String desc){
        this.code = code;
        this.alias = alias;
        this.desc = desc;
    }
}
