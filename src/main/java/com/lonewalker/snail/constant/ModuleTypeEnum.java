package com.lonewalker.snail.constant;

import lombok.Getter;

/**
 * @author LoneWalker
 */
@Getter
public enum ModuleTypeEnum {

    /**
     * eg:请假和加班属于假勤管理
     */
    FALSE_ATTENDANCE(1,"假勤管理"),
    /**
     * eg:转正和离职属于 人事管理
     */
    PERSONNEL(2,"人事管理"),


    ;

    private Integer code;

    private String desc;


    ModuleTypeEnum(Integer code,String desc){
        this.code = code;
        this.desc = desc;
    }

}
