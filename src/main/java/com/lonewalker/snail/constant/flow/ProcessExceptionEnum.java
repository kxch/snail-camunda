package com.lonewalker.snail.constant.flow;

import lombok.Getter;

/**
 * @author lonewalker
 */

@Getter
public enum ProcessExceptionEnum {

    CREATE_PROCESS_DEFINE_KEY_FAIL(90000,"创建流程定义Key失败，请联系管理员"),
    CREATE_PROCESS_DEFINE_FAIL(90001,"创建流程定义失败，请联系管理员"),
    DEPLOY_PROCESS_DEFINE_FAIL(90002,"发布流程定义失败，请联系管理员"),
    START_PROCESS_INSTANCE_FAIL(90003,"发起流程实例失败，请联系管理员"),
    SUBMIT_AGAIN_FAIL(90004,"流程再次提交失败，请联系管理员"),
    AGREE_TASK_FAIL(90005,"审批同意失败，请联系管理员"),
    REJECT_TASK_FAIL(90006,"审批驳回失败，请联系管理员"),
    TRANSFER_TASK_FAIL(90007,"审批转办失败，请联系管理员"),
    INITIATOR_RECALL_FAIL(90008,"发起人撤回失败，请联系管理员"),
    COPY_PROCESS_XML_FAIL(90009,"调用流程引擎复制xml服务失败,请联系管理员"),
    UPDATE_PROCESS_XML_FAIL(90010,"调用流程引擎更新xml服务失败,请联系管理员"),
    ADD_ASSIGNEE_FAIL(90011,"加签失败，请联系管理员"),
    QUERY_TO_DO_TASK_FAIL(90012,"引擎查询待办失败，请联系管理员"),
    QUERY_DONE_TASK_FAIL(90013,"引擎查询已办失败，请联系管理员"),
    QUERY_PROCESS_STATE_FAIL(90014,"查询流程状态失败，请联系管理员"),
    QUERY_NODE_INFO_FAIL(90015,"查询节点信息失败，请联系管理员"),
    QUERY_NODE_SATE_FAIL(90016,"查询节点状态失败，请联系管理员"),
    QUERY_UN_FINISHED_TASK_COUNT_FAIL(90017,"查询节点未完成任务数失败"),
    RECALL_APPROVAL_FAIL(90018,"审批撤回失败，请联系管理员"),



    ;
    private final Integer code;
    private final String desc;

    ProcessExceptionEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
