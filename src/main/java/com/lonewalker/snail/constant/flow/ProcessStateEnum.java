package com.lonewalker.snail.constant.flow;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author LoneWalker
 */

@Getter
@AllArgsConstructor
public enum ProcessStateEnum {

    /**
     * 0
     */
    NONE(0, "无"),

    /**
     * 尚未提交  非必要
     */
    WAIT_SUBMIT(1,"等待提交"),
    /**
     * 已提交 等待审核
     */
    WAIT_APPROVAL(2, "等待审批"),
    /**
     * 已有人审批  流程在审批中
     */
    APPROVAL(3,"审批中"),
    /**
     * 驳回
      */
    REJECT(4, "审批驳回"),
    /**
     * 流程结束
     */
    FINISHED(5, "审批通过"),
    /**
     * 流程被撤回
     */
    RECALL(6, "审批撤回"),

    ;
    private final Integer code;

    private final String value;


}
