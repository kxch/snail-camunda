package com.lonewalker.snail.constant.flow;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * bpmn相关元素枚举
 *
 * @author Lonewalker
 */

@Getter
@AllArgsConstructor
public enum BpmnEnum {
    EXPORTER(1000, "Camunda Modeler"),
    EXPORTER_VERSION(1001, "4.11.1"),
    TARGET_NAMESPACE(1002, "http://camunda.org/examples"),
    BPMN_DIAGRAM_ID(1003, "BPMNDiagram_1"),
    BPMN_PLANE_ID(1004, "BPMNPlane_1"),
    START_EVENT_ID(1005, "StartEvent_1"),
    SHAPE_START_ID(1006, "_BPMNShape_StartEvent_2"),
    END_EVENT_ID(1007, "EndEvent_1"),

    ;

    /**
     * 编码
     */
    private final Integer code;
    /**
     * 值
     */
    private final String value;
}
