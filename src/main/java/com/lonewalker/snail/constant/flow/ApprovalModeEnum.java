package com.lonewalker.snail.constant.flow;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 审批方式枚举
 *
 * @author Lonewalker
 */
@Getter
@AllArgsConstructor
public enum ApprovalModeEnum {

    /**
     * 或签
     */
    OR_SIGN(1),
    /**
     * 会签-并行审批
     */
    COUNTERSIGN(2),
    /**
     * 会签-串行审批
     */
    SEQUENTIAL(3),

    ;


    private final Integer code;
}
