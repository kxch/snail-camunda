package com.lonewalker.snail.util;

import com.lonewalker.snail.common.exception.ServiceInvokeException;


/**
 * @author lonewalker
 */
public class AssertUtil {

    /**
     * 服务调用异常 为true时抛异常
     * @param expression
     * @param message
     */
    public static void checkService(boolean expression, String message) {
        if (expression) {
            throw new ServiceInvokeException(message);
        }
    }
}
