package com.lonewalker.snail.util;

import com.lonewalker.snail.domain.entity.SysUser;


/**
 * 用户信息工具
 * @author lonewalker
 */
public class UserUtil {

    public static SysUser getUserInfo(){
        //这里是模拟登录后拿到当前用户信息
        return new SysUser(10088L,"Lin","林冲",1L);
    }

}
