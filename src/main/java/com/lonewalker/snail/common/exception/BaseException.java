package com.lonewalker.snail.common.exception;

/**
 * 基础异常
 *
 * @author lonewalker
 */
public class BaseException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    /**
     * 所属模块
     */
    private final String module;
    /**
     * 错误码
     */
    private final Integer code;
    /**
     * 错误信息
     */
    private final String message;

    public BaseException(String module, Integer code, String message) {
        this.module = module;
        this.code = code;
        this.message = message;
    }

    public BaseException(Integer code, String message) {
        this(null, code, message);
    }

    public BaseException(String message) {
        this(null, null, message);
    }

    public String getModule() {
        return module;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
