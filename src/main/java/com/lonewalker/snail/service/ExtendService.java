package com.lonewalker.snail.service;

import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.domain.request.AddAssigneeRequest;
import com.lonewalker.snail.domain.request.SequentialAddRequest;

/**
 * 扩展功能相关方法定义
 *
 * @author lonewalker
 */
public interface ExtendService {

    /**
     * 依次审批节点加签功能
     *
     * @param requestParam 请求参数
     * @return 提示信息
     */
    ApiResult<String> addAssignee(SequentialAddRequest requestParam);
}
