package com.lonewalker.snail.service;

import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.domain.entity.SysUser;
import com.lonewalker.snail.domain.request.ModifyInstanceRequest;
import com.lonewalker.snail.domain.request.RejectInstanceRequest;
import com.lonewalker.snail.domain.request.SetVariableRequest;
import com.lonewalker.snail.domain.request.StartProcessRequest;

import java.util.Map;

/**
 * @author lonewalker
 */
public interface ProcessInstanceService {

    /**
     * 根据流程定义key发起流程实例
     *
     * @param user    用户信息
     * @param request 参数
     * @return 流程实例id
     */
    ApiResult<String> startProcessInstanceByKey(SysUser user, StartProcessRequest request);

    /**
     * 根据流程定义id发起流程实例
     *
     * @param user    用户信息
     * @param request 参数
     * @return 流程实例id
     */
    ApiResult<String> startProcessInstanceById(SysUser user, StartProcessRequest request);

    /**
     * 驳回流程实例
     *
     * @param request 参数
     * @return 提示信息
     */
    ApiResult<String> rejectProcessInstance(RejectInstanceRequest request);

    /**
     * 设置流程实例变量
     *
     * @param request 参数
     * @return 提示信息
     */
    ApiResult<String> setVariableByInstance(SetVariableRequest request);

    /**
     * 挂起流程实例
     *
     * @param processInstanceId 流程实例id
     * @return 提示信息
     */
    ApiResult<String> suspendProcessInstanceById(String processInstanceId);

    /**
     * 激活流程实例
     *
     * @param processInstanceId 流程实例id
     * @return 提示信息
     */
    ApiResult<String> activateProcessInstanceById(String processInstanceId);

    /**
     * 删除流程实例
     *
     * @param processInstanceId 流程实例id
     * @return 提示信息
     */
    ApiResult<String> deleteProcessInstance(String processInstanceId);

    /**
     * 修改流程实例
     *
     * @param requestParam 请求参数
     * @return 提示信息
     */
    ApiResult<String> modifyProcessInstance(ModifyInstanceRequest requestParam);

    /**
     * 修改流程实例V2版本，适用于通过执行监听器获取审批人的方式
     *
     * @param requestParam 请求参数
     * @return 提示信息
     */
    ApiResult<String> modifyProcessInstanceV2(ModifyInstanceRequest requestParam);

    /**
     * 查询流程实例节点状态，只包含历史和当前节点
     *
     * @param processInstanceId 流程实例id
     * @return k:节点id v:状态值
     */
    ApiResult<Map<String, Integer>> queryInstanceNodeState(String processInstanceId);
}
