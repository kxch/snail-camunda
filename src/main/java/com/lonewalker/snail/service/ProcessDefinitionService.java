package com.lonewalker.snail.service;

import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.domain.entity.SysUser;
import com.lonewalker.snail.domain.request.CreateDefinitionRequest;
import com.lonewalker.snail.domain.request.DeployRequestParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author lonewalker
 */
public interface ProcessDefinitionService {

    /**
     * 发布流程定义
     * @param user          用户信息
     * @param requestParam  请求参数
     * @return 提示信息
     */
    ApiResult<String> deploy(SysUser user, DeployRequestParam requestParam);

    /**
     * 删除部署
     * @param deploymentId 部署id
     * @return 提示信息
     */
    ApiResult<String> deleteDeployment(String deploymentId);

    /**
     * 获取已部署的流程模型
     * @param processDefinitionId 流程定义id
     * @return 提示信息
     */
    ApiResult<String> getBpmnModelInstance(String processDefinitionId);

    /**
     * 挂起流程定义
     * @param processDefinitionId 流程定义id
     * @return 提示信息
     */
    ApiResult<String> suspendProcessDefinitionById(String processDefinitionId);

    /**
     * json转换为bpmn
     *
     * @param requestParam 相关参数
     * @return
     */
    ApiResult<String> convert(CreateDefinitionRequest requestParam);
}
