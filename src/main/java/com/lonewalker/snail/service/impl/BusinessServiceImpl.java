package com.lonewalker.snail.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.domain.entity.Business;
import com.lonewalker.snail.domain.entity.SysUser;
import com.lonewalker.snail.mapper.BusinessMapper;
import com.lonewalker.snail.domain.request.AddBusinessRequest;
import com.lonewalker.snail.service.BusinessService;
import com.lonewalker.snail.service.ProcessInstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @description: 业务服务类
 * @author: lonewalker
 * @create: 2022-12-23
 **/
@Service("businessService")
public class BusinessServiceImpl extends ServiceImpl<BusinessMapper, Business> implements BusinessService {

    @Autowired
    private ProcessInstanceService processInstanceService;

    /**
     * 这里模拟的是在发起流程实例时就把每个节点的审批人设置好
     * @param user
     * @param request
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ApiResult<Void> add(SysUser user, AddBusinessRequest request) {
        //先发起流程实例，发起成功才存业务数据
//        Map<String,Object> paramMap = new HashMap<>(4);
//        paramMap.put("initiator",user.getId().toString());
//        //通常会去查询当前用户的经理和部长
//        paramMap.put("manager","10087");
//        paramMap.put("head","10088");
//
//        StartProcessRequest startProcessRequest = new StartProcessRequest();
//        startProcessRequest.setProcessDefinitionKey(request.getProcessDefinitionKey());
//        startProcessRequest.setBusinessKey(BusinessTypeEnum.FORMAL.getAlias());
//        startProcessRequest.setTenantId(user.getTenantId().toString());
//        startProcessRequest.setParamMap(paramMap);
//
//        //发起流程
//        String processInstanceId = processInstanceService.startProcessInstance(user,startProcessRequest);
//
//        //保存业务数据
//        Business business = new Business();
//        business.setName(request.getName());
//        business.setProcessInstanceId(processInstanceId);
//        this.save(business);

        return ApiResult.success();
    }

    @Override
    public ApiResult<Void> update(SysUser user, AddBusinessRequest request) {
        return null;
    }
}
