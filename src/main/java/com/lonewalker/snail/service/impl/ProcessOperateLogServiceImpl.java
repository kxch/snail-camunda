package com.lonewalker.snail.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lonewalker.snail.domain.entity.ProcessOperateLog;
import com.lonewalker.snail.mapper.ProcessOperateLogMapper;
import com.lonewalker.snail.service.ProcessOperateLogService;
import org.springframework.stereotype.Service;

/**
 * @description: 流程操作日志服务
 * @author: lonewalker
 * @create: 2022-12-26
 **/
@Service("processOperateLogService")
public class ProcessOperateLogServiceImpl extends ServiceImpl<ProcessOperateLogMapper, ProcessOperateLog> implements ProcessOperateLogService {
}
