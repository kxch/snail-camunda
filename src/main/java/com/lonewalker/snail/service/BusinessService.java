package com.lonewalker.snail.service;

import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.domain.entity.SysUser;
import com.lonewalker.snail.domain.request.AddBusinessRequest;

public interface BusinessService {

    ApiResult<Void> add(SysUser user, AddBusinessRequest request);

    ApiResult<Void> update(SysUser user, AddBusinessRequest request);
}
