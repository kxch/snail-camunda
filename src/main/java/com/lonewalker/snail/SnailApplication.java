package com.lonewalker.snail;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lonewalker
 */
@MapperScan(basePackages = "com.lonewalker.snail.mapper")
@SpringBootApplication
public class SnailApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnailApplication.class, args);
    }

}
